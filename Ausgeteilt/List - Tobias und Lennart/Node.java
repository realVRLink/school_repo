
public class Node {
    Content inhalt;
    Node next;
    Node before;
    
    public Node(Content neuerInhalt) { inhalt=neuerInhalt; next = null;}
    
    public Node getNext() { return next; }
    public Node getBefore() { return before; }
    
    public Content getContent() { return inhalt; }
    public void setContent(Content c) { inhalt = c; }
    
    public void setNext(Node next) { this.next = next; }
    public void setBefore(Node before) { this.before = before; }
}
