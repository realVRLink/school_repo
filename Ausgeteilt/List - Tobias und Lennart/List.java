public class List {
    Node first;
    Node last;
    Node current;
    
    public List(){ first=null; last=null; current = null;}
    
    public void remove(){
       if (first == null){
           return;
        } else if(this.getLength() == 1) {
            first = null; last = null; current = null;
        } else if(current == first) {
            current = first.getNext();
            first = current;          
            current.setBefore(null);
        } else if(current == last) {
            current = last.getBefore();
            last = current; 
            current.setNext(null);
        } else {
            Node actN = current.getNext();
            Node actB = current.getBefore();
             
            actB.setNext(actN);
            actN.setBefore(actB);
            
            current = actB;
        }
    }
    
    public void append(Content c){
       if(first == null) {
           first = new Node(c);
           last = first;
           current = first;
        } else {
           last.setNext(new Node(c));
           last.getNext().setBefore(last);
           last = last.getNext();
        }
    }
    
    public void insert(Content c) {
        if(first == null) append(c);
        else if(current == first) {
            current.setBefore(new Node(c));
            first = current.getBefore();
            first.setNext(current);
            current = first;
        } else {
            Node actB = current.getBefore();
            
            actB.setNext(new Node(c));
            current.setBefore(actB.getNext());
            
            current.getBefore().setBefore(actB); 
            current.getBefore().setNext(current);
            current = current.getBefore(); 
        }        
    }
    
    public void toFirst() { if(first != null) current = first; }
    public void toLast() { if(last != null) current = last; }
    public void next() { if(current.getNext() != null) current = current.getNext(); }
    public void before() { if(current.getBefore() != null) current = current.getBefore(); }
    
    public boolean isEmpty() { return first == null ? true : false; }
    
    public void concat(List l1, List l2) {
        l1.last.setNext(l2.first);
        l2.first.setBefore(l1.last);
        last = l2.last;
    }
    
    public void setContent(Content c) { current.setContent(c); }
    public Content getContent() { return current.getContent(); }
    public Node getCurrent() { return current; }
    
            
    
    public void druckeVonVorne(){
       Node act=first;
       
       while(act != null){
           act.getContent().druckeInhalt();
           act = act.getNext();
       }
    }
    
    public int getLength(){
        int cnt;
        Node act = first;
        cnt =0;
        while(act !=null){
            cnt++;
            act=act.getNext();
        }    
        return cnt;
    }
    
    //AUFGABE:
    //Es soll die Ausgabe beginnend mit dem letzten Knoten auf den 
    //Bildschirm geschehen:
    /*public void druckeVonHinten(){
       int i,j;
       Node act;
       for (i= this.getLength();i>0;i--){
           
         for (j=1, act=first;j<=i;j++){
             act=act.getNext();
            }    
         act.getContent().druckeInhalt();   
        }
    }*/
    
    
}
