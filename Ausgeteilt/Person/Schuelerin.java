
/**
 * Beschreiben Sie hier die Klasse Schuelerin.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Schuelerin extends Person
{
    private int jgst;
    
    public Schuelerin(String name, int jahrgangsstufe){
       super(name);
       
       jgst = jahrgangsstufe;
    }
    
    public void druckeInfo(){
       druckeNamen();
       System.out.println("Jahrgang: "+jgst);
    }
}
