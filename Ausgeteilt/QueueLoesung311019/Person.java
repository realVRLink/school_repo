
/**
 * Beschreiben Sie hier die Klasse Person.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Person implements Content
{
   int id;
   String name;
   String vorname;
    
   public Person(String neuerName, String neuerVorname){
     name = neuerName;
     vorname = neuerVorname;
     id =0;
   }
   
   public void setId(int neueId){ id = neueId;}
   public int getId(){return id;}
   
   public String getName(){return name;}
   public String getVorname(){return vorname;}
   
   public void druckeInhalt(){
         
   }
    
   public String getString(){
      String inhaltsString= new String(name + ", " + vorname + " ID:"+id) ;
      return inhaltsString;
   }
}
