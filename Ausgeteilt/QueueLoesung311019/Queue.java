
/**
 * Beschreiben Sie hier die Klasse Queue.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Queue
{
    Node first;
    Node last;
    
    public Queue(){first=null; last=null;}
    
    public Content removeFirstContent(){
       if (first == null){
         return null;  
       } else{
         Content c= first.getContent(); 
         first = first.getNext(); 
         return c;
       }
    }
    
    public void fuegeAn(Content c){
       Node neuerKnoten = new Node(c);
       last.setNext(neuerKnoten);
       last=neuerKnoten;
    }
    
    public void druckeVonVorne(){
       Node act=first;
       
       while(act != null){
           act.getContent().druckeInhalt();
           act = act.getNext();
       }
    }
    
    public int getLength(){
        int cnt;
        Node act = first;
        cnt =0;
        while(act !=null){
            cnt++;
            act=act.getNext();
        }    
        return cnt;
    }
    
    //AUFGABE:
    //Es soll die Ausgabe beginnend mit dem letzten Knoten auf den 
    //Bildschirm geschehen:
    public void druckeVonHinten(){
       int i,j;
       Node act;
       for (i= this.getLength();i>0;i--){
           
         for (j=1, act=first;j<=i;j++){
             act=act.getNext();
            }    
         act.getContent().druckeInhalt();   
        }
    }
    
    
}
