
/**
 * Beschreiben Sie hier die Klasse Node.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Node
{
    Content inhalt;
    Node next;
    public Node(Content neuerInhalt){inhalt=neuerInhalt; next = null;}
    
    public Node getNext(){return next;}
    
    public Content getContent(){return inhalt;}
    
    public void setNext(Node n){next =n;}
}
